export const API_URL = 'http://localhost:8080'
export const LOG_IN_URL = `${API_URL}/login`
export const LOG_OUT_URL = `${API_URL}/logout`
export const REGISTER_URL = `${API_URL}/register`
export const UPDATE_SENTENCE_URL = `${API_URL}/updateSentence`
