import React from 'react'
import './App.css'
import Header from 'src/Header/Header'
import Footer from 'src/Footer/Footer'
import Page from 'src/components/Page/Page'

function App() {
  return (
      <div className='App'>
        <Header/>
        <Page/>
        <Footer/>
      </div>
  )
}

export default App