import React from 'react'
import { makeStyles } from '@material-ui/styles'
import CopyrightIcon from '@material-ui/icons/Copyright'

const useStyles = makeStyles(() => ({
  footer: {
    position: 'absolute',
    left: '0',
    bottom: '0',
    right: '0',
    padding: '1rem',
    textAlign: 'center',
    fontSize: '24px'
  }
}))

function Footer() {
  const classes = useStyles()
  return (
      <div className={classes.footer}>
        <CopyrightIcon/> Michał Maciejewski
      </div>
  )
}

export default Footer