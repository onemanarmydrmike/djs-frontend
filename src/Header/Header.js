import React from 'react'
import { makeStyles } from '@material-ui/styles'

const useStyles = makeStyles(() => ({
  header: {
    position: 'absolute',
    left: '0',
    top: '0',
    right: '0',
    padding: '1rem',
    textAlign: 'center'
  }
}))

function Header() {
  const classes = useStyles()
  return (
      <div className={classes.header}>
        Hello World!<br/>
        This is DJS Lab project created for university purpose
      </div>
  )
}

export default Header