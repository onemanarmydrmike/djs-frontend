import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Button, TextField } from '@material-ui/core'
import { Edit as EditIcon } from '@material-ui/icons'
import { callSentenceUpdate } from 'src/store/actions'

class Content extends Component {
  constructor(props) {
    super(props)
    this.state = {
      editable: false,
      newSentence: props.user.sentence
    }
  }

  handleSentenceChange = (input) => {
    this.setState({
      newSentence: input
    })
  }

  saveSentenceChange = () => {
    this.props.submitSentenceChange(this.props.user, this.state.newSentence)
    this.setState({ editable: false })
  }

  onEditSentenceButtonClick = () => {
    const { editable } = this.state
    editable === true
        ? this.saveSentenceChange()
        : this.setState({
            editable: true,
            newSentence: this.props.user.sentence
          })
  }

  render() {
    const {
      user,
      loggedIn
    } = this.props
    return (
        <>
          <div>username: {user.username}</div>
          <span>
            {
              loggedIn &&
              <Button onClick={this.onEditSentenceButtonClick}>
                <EditIcon color='primary'/>
              </Button>
            }
            {
              this.state.editable === true
                  ? <span>sentence: <TextField value={this.state.newSentence} onChange={evt => this.handleSentenceChange(evt.target.value)}/></span>
                  : <span>sentence: {user.sentence}</span>
            }
          </span>
        </>
    )
  }
}

const mapStateToProps = state => state

const mapDispatcherToProps = dispatch => {
  return {
    submitSentenceChange: (user, newSentence) => dispatch(callSentenceUpdate(user, newSentence))
  }
}

export default connect(mapStateToProps, mapDispatcherToProps)(Content)