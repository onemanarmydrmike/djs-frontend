import React, { Component } from 'react'
import {
  Button,
  TextField,
  Dialog as MUIDialog,
  DialogContent as MUIDialogContent,
  DialogActions as MUIDialogActions
} from '@material-ui/core'
import cloneDeep from 'lodash/cloneDeep'
import DialogType from 'src/models/DialogType'

class Dialog extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isPasswordValid: true,
      isConfirmPasswordValid: true,
      userForm: {
        username: '',
        password: '',
        confirmPassword: ''
      }
    }
  }

  onInputChange = (evt) => {
    let newUserForm = cloneDeep(this.state.userForm)
    newUserForm[ evt.target.id ] = evt.target.value
    this.setState({
      userForm: newUserForm
    })
  }

  closeDialog = () => {
    this.setState({
      userForm: {
        username: '',
        password: '',
        confirmPassword: ''
      },
      isPasswordValid: true,
      isConfirmPasswordValid: true
    })
    this.props.closeDialog()
  }

  validatePassword = () => {
    let isPasswordValid = true
    if (this.props.type === DialogType.registration) {
      const matchArray = this.state.userForm.password.match('^(?=.*?[A-Z])(?=.*?[#?!@$%^&*-]).{5,}$')
      isPasswordValid = matchArray && matchArray.length > 0
    }
    this.setState({
      isPasswordValid: isPasswordValid
    })
  }

  validateConfirmPassword = (evt) => {
    const isConfirmPasswordValid = this.state.userForm.password === evt.target.value
    this.setState({
      isConfirmPasswordValid: isConfirmPasswordValid
    })
  }

  submitLogIn = (user) => {
    this.props.onSubmitLogIn(user)
    this.closeDialog()
  }

  submitRegistration = (userForm) => {
    this.props.onSubmitRegistration(userForm)
    this.closeDialog()
  }

  render() {
    const {
      type,
      open
    } = this.props

    const {
      isPasswordValid,
      isConfirmPasswordValid,
      userForm
    } = this.state

    const submitButtonLabel = type === DialogType.login ? 'log in' : 'register'
    const isRegistrationDialog = type === DialogType.registration
    const isSubmitButtonDisabled = isRegistrationDialog && (!isPasswordValid || !isConfirmPasswordValid || !userForm.confirmPassword || !userForm.password)

    return (
        <MUIDialog open={open} onClose={this.closeDialog}>
          <MUIDialogContent>
            <TextField
                id='username'
                label='Username'
                value={userForm.username}
                onChange={evt => this.onInputChange(evt)}
                margin='normal'
                required
            />
            <br/>
            <TextField
                id='password'
                label='Password'
                value={userForm.password}
                onChange={evt => this.onInputChange(evt)}
                onBlur={this.validatePassword}
                type='password'
                margin='normal'
                error={!isPasswordValid}
                helperText={isPasswordValid ? '' : 'min 5 chars with 1 UPPERCASE letter and 1 special character required'}
                required
            />
            {
              isRegistrationDialog &&
              <TextField
                  id='confirmPassword'
                  label='Confirm password'
                  value={userForm.confirmPassword}
                  onChange={evt => {
                    this.onInputChange(evt)
                    this.validateConfirmPassword(evt)
                  }}
                  type='password'
                  margin='normal'
                  style={{ marginLeft: '20px' }}
                  error={!isConfirmPasswordValid}
                  helperText={isConfirmPasswordValid ? '' : 'passwords are not equal'}
                  required={isRegistrationDialog}
              />
            }
          </MUIDialogContent>

          <MUIDialogActions>
            <Button
                variant='contained'
                size='large'
                onClick={this.closeDialog}
            >
              Cancel
            </Button>
            <Button
                variant='contained'
                color='primary'
                size='large'
                onClick={isRegistrationDialog
                    ? () => this.submitRegistration(userForm)
                    : () => this.submitLogIn(userForm)
                }
                disabled={isSubmitButtonDisabled}
            >
              {submitButtonLabel}
            </Button>
          </MUIDialogActions>
        </MUIDialog>
    )
  }
}

export default Dialog