import React from 'react'
import { Grid } from '@material-ui/core'
import Content from './Content/Content'
import RightPane from './RightPane/RightPane'

function Page() {
  return (
      <Grid container>
        <Grid item xs={9}>
          <Content/>
        </Grid>
        <Grid item xs={3}>
          <RightPane/>
        </Grid>
      </Grid>
  )
}

export default Page