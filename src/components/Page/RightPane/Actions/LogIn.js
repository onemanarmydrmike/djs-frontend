import React, { Component } from 'react'
import { Button } from '@material-ui/core'
import { connect } from 'react-redux'
import { logInUser } from 'src/store/actions'
import Dialog from 'src/components/Page/Content/Dialog/Dialog'
import DialogType from '../../../../models/DialogType'

class LogIn extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isDialogOpen: false
    }
  }

  openDialog = () => {
    this.setState({
      isDialogOpen: true
    })
  }

  closeDialog = () => {
    this.setState({
      isDialogOpen: false
    })
  }

  render() {
    return (
        <>
          {
            !this.props.loggedIn &&
            <Button
                style={this.props.buttonStyle}
                variant='contained'
                color='primary'
                onClick={this.openDialog}
            >
              log in
            </Button>
          }
          <Dialog
              type={DialogType.login}
              closeDialog={this.closeDialog}
              onSubmitLogIn={this.props.onSubmit}
              open={this.state.isDialogOpen}
          />
        </>
    )
  }
}

const mapStateToProps = state => state

const mapDispatcherToProps = dispatch => {
  return {
    onSubmit: (user) => dispatch(logInUser(user))
  }
}

export default connect(mapStateToProps, mapDispatcherToProps)(LogIn)