import React from 'react'
import { Button } from '@material-ui/core'
import { connect } from 'react-redux'
import { logOutUser } from 'src/store/actions'

function LogOut(props) {
  return (
      props.loggedIn &&
      <Button
          style={props.buttonStyle}
          variant='contained'
          color='primary'
          onClick={props.onClick}
      >
        log out
      </Button>
  )
}

const mapStateToProps = state => state

const mapDispatcherToProps = dispatch => {
  return {
    onClick: () => dispatch(logOutUser())
  }
}

export default connect(mapStateToProps, mapDispatcherToProps)(LogOut)