import React, { Component } from 'react'
import { Button } from '@material-ui/core'
import { connect } from 'react-redux'
import { registerUser } from 'src/store/actions'
import Dialog from 'src/components/Page/Content/Dialog/Dialog'
import DialogType from 'src/models/DialogType'

class Register extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isDialogOpen: false
    }
  }

  openDialog = () => {
    this.setState({
      isDialogOpen: true
    })
  }

  closeDialog = () => {
    this.setState({
      isDialogOpen: false
    })
  }

  render() {
    return (
        <>
          {
            !this.props.loggedIn &&
            <Button
                style={this.props.buttonStyle}
                variant='contained'
                color='primary'
                onClick={this.openDialog}
            >
              register
            </Button>
          }
          <Dialog
              type={DialogType.registration}
              closeDialog={this.closeDialog}
              onSubmitRegistration={this.props.onSubmit}
              open={this.state.isDialogOpen}
          />
        </>
    )
  }
}

const mapStateToProps = state => state

const mapDispatcherToProps = dispatch => {
  return {
    onSubmit: (user) => dispatch(registerUser(user))
  }
}

export default connect(mapStateToProps, mapDispatcherToProps)(Register)