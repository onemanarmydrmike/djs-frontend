import React, { Component } from 'react'
import Login from './Actions/LogIn'
import LogOut from './Actions/LogOut'
import Register from './Actions/Register'
import { connect } from 'react-redux'

const buttonStyle = {
  width: '80%',
  marginBottom: '2rem'
}

const feedbackStyle = {
  color: 'red',
  size: 14
}

class RightPane extends Component {
  render() {
    return (
        <div>
          <Login buttonStyle={buttonStyle}/>
          <LogOut buttonStyle={buttonStyle}/>
          <Register buttonStyle={buttonStyle}/>
          <p style={feedbackStyle}>{this.props.registrationFeedback}</p>
        </div>
    )
  }
}

const mapStateToProps = state => state

export default connect(mapStateToProps)(RightPane)