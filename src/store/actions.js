import withQuery from 'with-query'

import { LOG_IN_URL, LOG_OUT_URL, REGISTER_URL, UPDATE_SENTENCE_URL } from 'src/ApiConstants'

export const LOGIN = 'LOGIN'
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
export const LOGIN_FAILED = 'LOGIN_FAILED'

export const LOGOUT = 'LOGOUT'
export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS'
export const LOGOUT_FAILED = 'LOGOUT_FAILED'

export const REGISTER = 'REGISTER'
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS'
export const REGISTER_FAILED = 'REGISTER_FAILED'

export const UPDATE_SENTENCE = 'UPDATE_SENTENCE'
export const UPDATE_SENTENCE_SUCCESS = 'UPDATE_SENTENCE_SUCCESS'
export const UPDATE_SENTENCE_FAILED = 'UPDATE_SENTENCE_FAILED'

export function logIn() {
  return {
    type: LOGIN
  }
}

export function logInSuccess(user) {
  return {
    type: LOGIN_SUCCESS,
    loggedIn: true,
    user: user
  }
}

export function logOut() {
  return {
    type: LOGOUT
  }
}

export function logOutSuccess() {
  return {
    type: LOGOUT_SUCCESS,
    loggedIn: false
  }
}

export function register() {
  return {
    type: REGISTER
  }
}

export function registerSuccess(json) {
  return {
    type: REGISTER_SUCCESS,
    registrationFeedback: json.username ? `user ${json.username} registered successfully` : json.error
  }
}

export function updateSentence() {
  return {
    type: UPDATE_SENTENCE
  }
}

export function updateSentenceSuccess(response) {
  return {
    type: UPDATE_SENTENCE_SUCCESS,
    user: response
  }
}

export function logInUser(user) {
  return dispatch => {
    dispatch(logIn())

    fetch(LOG_IN_URL, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        username: user.username,
        password: user.password,
      })
    })
    .then(response => response.json())
    .then(json => dispatch(logInSuccess(json)))
    .catch(error => console.log(error))
  }
}

export function logOutUser() {
  return dispatch => {
    dispatch(logOut())

    fetch(LOG_OUT_URL, {
      method: 'POST'
    })
    .then(dispatch(logOutSuccess()))
    .catch(error => console.log(error))
  }
}

export function registerUser(user) {
  return dispatch => {
    dispatch(register())

    fetch(REGISTER_URL, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        username: user.username,
        password: user.password
      })
    })
    .then(response => response.json())
    .then(json => dispatch(registerSuccess(json)))
    .catch(error => console.log(error))
  }
}

export function callSentenceUpdate(user, newSentence) {
  return dispatch => {
    dispatch(updateSentence())

    fetch(withQuery(UPDATE_SENTENCE_URL), {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        username: user.username,
        password: user.password,
        sentence: newSentence,
      })
    })
    .then(response => response.json())
    .then(json => {
      dispatch(updateSentenceSuccess(json))
    })
    .catch(error => console.log(error))
  }
}