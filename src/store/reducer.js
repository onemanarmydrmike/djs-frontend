import * as actions from './actions'
import { initialState } from 'src/store/store'

const reducer = (state, action) => {
  switch (action.type) {
    case actions.LOGIN:
    case actions.LOGOUT:
    case actions.REGISTER:
    case actions.UPDATE_SENTENCE:
      return {
        ...state,
        isRequestInProgress: true
      }
    case actions.LOGIN_SUCCESS:
      return {
        ...state,
        isRequestInProgress: false,
        loggedIn: true,
        user: action.user
      }
    case actions.LOGOUT_SUCCESS:
      return {
        ...state,
        isRequestInProgress: false,
        loggedIn: false,
        user: initialState.user
      }
    case actions.REGISTER_SUCCESS:
      return {
        ...state,
        isRequestInProgress: false,
        registrationFeedback: action.registrationFeedback
      }
    case actions.UPDATE_SENTENCE_SUCCESS:
      return {
        ...state,
        isRequestInProgress: false,
        user: action.user
      }
    case actions.LOGIN_FAILED:
    case actions.LOGOUT_FAILED:
    case actions.REGISTER_FAILED:
    case actions.UPDATE_SENTENCE_FAILED:
      return {
        ...state,
        isRequestInProgress: false,
        error: action.error
      }
    default:
      return state
  }
}

export default reducer