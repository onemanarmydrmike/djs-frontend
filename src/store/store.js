import { applyMiddleware, createStore } from 'redux'
import thunk from 'redux-thunk'
import reducer from './reducer'

export const initialState = {
  isRequestInProgress: false,
  user: {
    username: 'guest',
    sentence: 'log in to modify...'
  },
  loggedIn: false,
  registrationFeedback: ''
}

const store = createStore(reducer, initialState, applyMiddleware(thunk))

export default store